-- Your SQL goes here
CREATE TABLE `chapters` (
    `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `title` TEXT NOT NULL,
    `path` TEXT NOT NULL UNIQUE,
    `cover_path` TEXT NULL UNIQUE,
    `created_date` TIMESTAMP
    DEFAULT CURRENT_TIMESTAMP
);
