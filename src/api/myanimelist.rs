extern crate nanoid;
extern crate serde_json;
use super::resource::WebResource;
use failure::Error;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
struct Root {
    request_hash: String,
    request_cached: bool,
    request_cache_expiry: i64,
    top: Vec<Manga>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Manga {
    mal_id: i64,
    rank: i64,
    pub title: String,
    pub url: String,
    #[serde(rename = "type")]
    type_field: String,
    volumes: Option<i64>,
    start_date: String,
    end_date: Option<String>,
    members: i64,
    score: f64,
    image_url: String,
}

pub fn get_top_manga() -> Result<Vec<WebResource>, Error> {
    let api_uri = "https://api.jikan.moe/v3/top/manga/1/bypopularity";
    let client = reqwest::Client::new();
    let mut response = client.get(api_uri).send()?;

    let root = response.json::<Root>()?;

    Ok(root.top.into_iter().map(|manga| WebResource::from(manga)).collect())
}

impl From<Manga> for WebResource {
    fn from(manga: Manga) -> Self {
        WebResource {
            title: manga.title,
            start_date: Some(manga.start_date),
            cover_image: manga.image_url,
            url: manga.url,
        }
    }
}
