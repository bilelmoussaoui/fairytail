extern crate nanoid;
extern crate serde_json;
use super::resource::WebResource;
use failure::Error;

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
struct Root {
    error: String,
    limit: i64,
    offset: i64,
    number_of_page_results: i64,
    number_of_total_results: i64,
    status_code: i64,
    results: Vec<Comics>,
    version: String,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Comics {
    aliases: Option<String>,
    api_detail_url: String,
    count_of_issues: i64,
    date_added: String,
    date_last_updated: String,
    deck: Option<String>,
    description: Option<String>,
    first_issue: Option<FirstIssue>,
    id: i64,
    image: Image,
    last_issue: Option<LastIssue>,
    pub name: String,
    publisher: Option<Publisher>,
    pub site_detail_url: String,
    start_year: Option<String>,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
struct FirstIssue {
    api_detail_url: String,
    id: i64,
    name: Option<String>,
    issue_number: String,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
struct Image {
    icon_url: String,
    medium_url: String,
    screen_url: String,
    screen_large_url: String,
    small_url: String,
    super_url: String,
    thumb_url: String,
    tiny_url: String,
    original_url: String,
    image_tags: String,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
struct LastIssue {
    api_detail_url: String,
    id: i64,
    name: Option<String>,
    issue_number: String,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
struct Publisher {
    api_detail_url: String,
    id: i64,
    name: String,
}

pub fn get_top_comics() -> Result<Vec<WebResource>, Error> {
    let api_uri = "https://comicvine.gamespot.com/api/volumes/?api_key=349d3dfbf80c1a5f0f25dbf5078b098351772793&format=json&sort=date_last_updated:desc";
    let client = reqwest::Client::new();
    let mut response = client.get(api_uri).send()?;

    let root = response.json::<Root>()?;

    Ok(root.results.into_iter().map(|comics| WebResource::from(comics)).collect())
}

impl From<Comics> for WebResource {
    fn from(comics: Comics) -> Self {
        WebResource {
            title: comics.name,
            start_date: comics.start_year,
            cover_image: comics.image.medium_url,
            url: comics.api_detail_url,
        }
    }
}
