mod comicvine;
mod myanimelist;
mod resource;

pub use comicvine::{get_top_comics, Comics};
pub use myanimelist::{get_top_manga, Manga};
pub use resource::WebResource;
