use crate::models::Viewable;
use failure::Error;
use std::fs;
use std::fs::File;
use std::path::PathBuf;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct WebResource {
    pub title: String,
    pub start_date: Option<String>,
    pub cover_image: String,
    pub url: String,
}

impl WebResource {
    pub fn get_created_at(&self) -> Option<String> {
        self.start_date.clone()
    }
}

impl Viewable for WebResource {
    fn get_cover_image(&self) -> Result<PathBuf, Error> {
        let mut cache_file: PathBuf = [glib::get_user_cache_dir().unwrap().to_str().unwrap(), "fairytale", "discovery"].iter().collect();
        fs::create_dir_all(&cache_file)?;
        cache_file.push(self.get_title().replace(" ", ""));
        if !cache_file.exists() {
            let mut resp = reqwest::get(&self.cover_image)?;
            match resp.status() {
                reqwest::StatusCode::OK => {
                    let mut dest = File::create(&cache_file)?;
                    resp.copy_to(&mut dest)?;
                }
                _ => bail!("Failed to download {}", self.cover_image),
            }
        }
        Ok(cache_file)
    }

    fn get_title(&self) -> String {
        self.title.clone()
    }
}
