use crate::application::Action;
use glib::Sender;
use gtk::prelude::*;

pub struct LibraryFlowBox {
    pub widget: gtk::Box,
    sender: Sender<Action>,
    flowbox: gtk::FlowBox,
}

impl LibraryFlowBox {
    pub fn new(sender: Sender<Action>) -> Self {
        let flowbox = Self {
            widget: gtk::Box::new(gtk::Orientation::Vertical, 0),
            sender,
            flowbox: gtk::FlowBox::new(),
        };
        flowbox.init();
        flowbox
    }

    pub fn new_with_label(label: &str, sender: Sender<Action>) -> Self {
        let flowbox = LibraryFlowBox::new(sender);
        let label_widget = gtk::Label::new(Some(label));
        label_widget.set_halign(gtk::Align::Start);
        label_widget.get_style_context().add_class("library-title");
        label_widget.show();
        flowbox.widget.pack_start(&label_widget, false, false, 0);
        flowbox
    }

    fn init(&self) {
        self.flowbox.connect_size_allocate(move |widget, allocation| {
            if allocation.width < 1000 {
                widget.set_max_children_per_line(4);
                widget.set_row_spacing(24);
                widget.set_column_spacing(24);
            } else if allocation.width < 600 {
                widget.set_max_children_per_line(3);
                widget.set_row_spacing(12);
                widget.set_column_spacing(12);
            } else if allocation.width < 430 {
                widget.set_max_children_per_line(2);
                widget.set_row_spacing(0);
                widget.set_column_spacing(0);
            } else {
                widget.set_max_children_per_line(6);
                widget.set_row_spacing(32);
                widget.set_column_spacing(32);
            }
        });
        self.flowbox.set_hexpand(true);
        self.flowbox.set_halign(gtk::Align::Fill);
        self.flowbox.set_valign(gtk::Align::Fill);
        self.flowbox.set_orientation(gtk::Orientation::Horizontal);
        self.flowbox.set_min_children_per_line(2);
        self.flowbox.set_max_children_per_line(8);
        self.flowbox.set_homogeneous(true);
        self.flowbox.set_selection_mode(gtk::SelectionMode::None);
        self.flowbox.get_style_context().add_class("library");
        self.flowbox.show();
        self.widget.pack_end(&self.flowbox, true, true, 0);
        self.widget.show();
    }

    pub fn bind_model<F>(&self, model: &gio::ListStore, callback: F)
    where
        for<'r> F: std::ops::Fn(&'r glib::Object) -> gtk::Widget + 'static,
    {
        self.flowbox.bind_model(Some(model), callback);
    }
}
