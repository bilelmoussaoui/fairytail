use super::library_flowbox::LibraryFlowBox;
use super::library_row::LibraryRow;
use crate::application::Action;
use crate::models::{Chapter, NewChapter, ObjectWrapper, Paginable};
use crate::models::{DirChapter, LibraryModel, RarChapter, ZipChapter};
use crate::models::{SortBy, SortOrder};
use failure::Error;
use gio::prelude::*;
use glib::Sender;
use gtk::prelude::*;
use std::path::Path;
use std::rc::Rc;
pub struct Library {
    pub widget: libhandy::Column,
    builder: gtk::Builder,
    model: LibraryModel,
    sender: Sender<Action>,
    sort_order: SortOrder,
    sort_by: SortBy,
}

impl Library {
    pub fn new(sender: Sender<Action>) -> Rc<Self> {
        let model = LibraryModel::new();
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/FairyTale/library.ui");
        let widget = builder.get_object("library").expect("Failed to load library object");

        let library = Rc::new(Library {
            widget,
            builder,
            model,
            sender,
            sort_order: SortOrder::Asc,
            sort_by: SortBy::Name,
        });
        library.init(library.clone());
        library.refresh_view();
        library
    }

    pub fn get_chapter_from_path(&self, path: &str) -> Result<Option<Box<Paginable>>, Error> {
        let file = gio::File::new_for_path(&path);
        let info = file.query_info("standard::content-type", gio::FileQueryInfoFlags::NONE, gio::NONE_CANCELLABLE)?;

        let chapter: Box<Paginable> = {
            match info.get_content_type().unwrap().as_str() {
                "application/zip" => Box::new(ZipChapter::new(path.to_string())?),
                "inode/directory" => Box::new(DirChapter::new(path.to_string())?),
                "application/vnd.comicbook-rar" => Box::new(RarChapter::new(path.to_string())?),
                _ => bail!("Unsupported file format"),
            }
        };
        Ok(Some(chapter))
    }

    pub fn add_chapter(&self, path: &str) -> Result<Option<Box<Paginable>>, Error> {
        let mut chapter = self.get_chapter_from_path(path)?.expect("Failed to load episode");
        if chapter.total_pages() == 0 {
            bail!("Empty file");
        } else {
            let cover = chapter.get_cover().unwrap();
            let std_path = Path::new(path);
            let file = gio::File::new_for_path(std_path);

            let info = file.query_info("standard::display-name", gio::FileQueryInfoFlags::NONE, gio::NONE_CANCELLABLE).unwrap();
            let display_info = info.get_attribute_string("standard::display-name");

            let display_name = display_info.unwrap().as_str().to_string();

            let new_chapter = NewChapter {
                title: display_name,
                path: path.to_string(),
                cover_path: cover.to_str().unwrap().to_string(),
            };
            self.model.add_item(new_chapter)?;
            Ok(Some(chapter))
        }
    }

    pub fn refresh_view(&self) {
        let library_stack: gtk::Stack = self.builder.get_object("library_stack").unwrap();
        if self.model.get_count() != 0 {
            library_stack.set_visible_child_name("chapters");
        } else {
            library_stack.set_visible_child_name("empty");
        }
    }

    fn init(&self, lib: Rc<Self>) {
        let viewport: gtk::Viewport = self.builder.get_object("viewport").unwrap();
        let flowbox = LibraryFlowBox::new(self.sender.clone());
        let sender = self.sender.clone();
        flowbox.bind_model(&self.model.model, move |chapter| {
            let chapter: Chapter = chapter.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
            let row = LibraryRow::new(Box::new(chapter.clone()), sender.clone());
            let sender = sender.clone();
            row.set_on_click_callback(move |_, _| {
                sender.send(Action::LoadChapter(chapter.clone())).unwrap();
                gtk::Inhibit(false)
            });
            row.widget.upcast::<gtk::Widget>()
        });
        viewport.add(&flowbox.widget);

        self.model.model.connect_items_changed(move |_, _, _, _| {
            lib.refresh_view();
        });
    }

    pub fn sort(&self, sort_by: Option<SortBy>, sort_order: Option<SortOrder>) {
        self.model.set_sorting(sort_by, sort_order);
    }
}
