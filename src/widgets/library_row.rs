use gtk::prelude::*;

use super::cover::CoverImage;
use crate::application::Action;
use crate::models::Viewable;
use glib::Sender;
use std::cell::RefCell;

pub struct LibraryRow {
    pub widget: gtk::FlowBoxChild,
    builder: gtk::Builder,
    sender: Sender<Action>,
}

impl LibraryRow {
    pub fn new(viewable: Box<Viewable>, sender: Sender<Action>) -> Self {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/FairyTale/library_row.ui");
        let widget: gtk::FlowBoxChild = builder.get_object("library_row").expect("Failed to load library_row object");

        let library_row = Self { widget, builder, sender };
        library_row.init(viewable);
        library_row
    }

    fn init(&self, viewable: Box<Viewable>) {
        let label: gtk::Label = self.builder.get_object("label").expect("Failed to load chapter label");
        label.set_text(&viewable.get_title());

        let stack: gtk::Stack = self.builder.get_object("stack").expect("Failed to load stack");
        stack.set_visible_child_name("cover");
        let cover_container: gtk::Box = self.builder.get_object("cover_container").expect("Failed to load library row cover_container");

        let mut pixbuf: Option<gdk_pixbuf::Pixbuf> = None;
        match viewable.get_cover_image() {
            Ok(cache_file) => {
                pixbuf = Some(gdk_pixbuf::Pixbuf::new_from_file(cache_file).unwrap());
            }
            Err(_) => {
                warn!("Failed to download image");
            }
        }
        let cover_image = CoverImage::new(pixbuf);
        cover_container.pack_start(&cover_image.widget, true, true, 0);
    }

    pub fn set_on_click_callback<F>(&self, callback: F)
    where
        for<'r, 's> F: std::ops::Fn(&'r gtk::EventBox, &'s gdk::EventButton) -> gtk::Inhibit + 'static,
    {
        let event_box: gtk::EventBox = self.builder.get_object("eventbox").expect("Failed to load eventbox");
        event_box.connect_button_press_event(callback);
    }
}
