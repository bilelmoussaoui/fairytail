use super::discovery::Discovery;
use super::library::Library;
use super::reader::Reader;
use crate::application::Action;
use crate::config::{APP_ID, PROFILE};
use crate::models::{Chapter, Paginable, SortBy, SortOrder, Viewable};
use crate::window_state;
use gio::prelude::*;
use glib::Sender;
use gtk::prelude::*;
use libhandy::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(PartialEq, Debug)]
pub enum View {
    Reader,
    Library,
}
const TARGET_URI: u32 = 0;

pub struct Window {
    pub widget: gtk::ApplicationWindow,
    sender: Sender<Action>,
    builder: gtk::Builder,
    settings: RefCell<gio::Settings>,
    pub reader: Rc<RefCell<Reader>>,
    library: RefCell<Rc<Library>>,
    discovery: RefCell<Rc<Discovery>>,
}

impl Window {
    pub fn new(sender: Sender<Action>) -> Rc<RefCell<Self>> {
        let settings = gio::Settings::new(APP_ID);
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/FairyTale/window.ui");
        let widget: gtk::ApplicationWindow = builder.get_object("window").unwrap();
        let reader = Reader::new();
        let library = Library::new(sender.clone());
        let discovery = Discovery::new(sender.clone());

        if PROFILE == "Devel" {
            widget.get_style_context().add_class("devel");
        }
        let window = Rc::new(RefCell::new(Window {
            widget,
            sender,
            builder,
            settings: RefCell::new(settings),
            reader,
            library: RefCell::new(library),
            discovery: RefCell::new(Rc::new(discovery)),
        }));

        window.borrow().init();
        window.borrow().setup_actions(window.clone());
        window.borrow().set_view(View::Library);
        window
    }

    pub fn set_view(&self, view: View) {
        let main_stack: gtk::Stack = self.builder.get_object("main_stack").unwrap();
        let header_stack: gtk::Stack = self.builder.get_object("header_stack").unwrap();
        let menu_stack: gtk::Stack = self.builder.get_object("menu_stack").unwrap();
        match view {
            View::Reader => {
                main_stack.set_visible_child_name("reader");
                header_stack.set_visible_child_name("reader");
                menu_stack.set_visible_child_name("reader");
            }
            View::Library => {
                main_stack.set_visible_child_name("library");
                header_stack.set_visible_child_name("library");
                menu_stack.set_visible_child_name("library");
            }
        }
    }

    pub fn add_to_library(&self, path: &str) {
        // Add a comics to the library and load it
        match self.library.borrow_mut().add_chapter(path) {
            Ok(chapter) => {
                self.load_chapter(chapter);
            }
            Err(err) => {
                // TODO: show a error stack
                error!("Failed to load episode {}", err);
            }
        }
    }

    pub fn load(&self, chapter: Rc<Box<Viewable>>) {
        println!("loading something");
    }

    pub fn read_chapter(&self, chapter: Chapter) {
        let chapter = self.library.borrow().get_chapter_from_path(&chapter.path).expect("Chapter not found");
        self.load_chapter(chapter);
    }

    fn load_chapter(&self, chapter: Option<Box<Paginable>>) {
        if let Some(chapter) = chapter {
            let chapter_label: gtk::Label = self.builder.get_object("chapter_label").unwrap();

            // chapter_label.set_text(&chapter.get_title());

            self.reader.borrow_mut().load(chapter);
            self.sender.send(Action::ViewReader).unwrap();
        }
    }

    fn init(&self) {
        // load latest window state
        let settings = self.settings.borrow().clone();
        window_state::load(&self.widget, &settings);
        // save window state on delete event
        self.widget.connect_delete_event(move |window, _| {
            window_state::save(&window, &settings);
            Inhibit(false)
        });

        let main_stack: gtk::Stack = self.builder.get_object("main_stack").unwrap();

        let uri_target = gtk::TargetEntry::new("text/uri-list", gtk::TargetFlags::OTHER_APP, TARGET_URI);
        main_stack.drag_dest_set(gtk::DestDefaults::ALL, &[uri_target], gdk::DragAction::COPY);

        let sender = self.sender.clone();
        main_stack.connect_drag_data_received(move |_, _, _, _, data, target_id, _| {
            if target_id == TARGET_URI {
                let uris = data.get_uris();
                if let Some(dropped_uri) = uris.get(0) {
                    let uri = dropped_uri.as_str().to_string();
                    sender.send(Action::LoadURI(uri)).unwrap();
                }
            }
        });

        let reader_container: gtk::Box = self.builder.get_object("reader_container").unwrap();
        reader_container.pack_start(&self.reader.borrow().widget, true, true, 0);

        let library_stack: gtk::Stack = self.builder.get_object("library_stack").unwrap();
        let library = self.library.borrow().widget.clone();
        let discovery = self.discovery.borrow().widget.clone();

        library_stack.add_titled(&library, "library", "Library");
        library_stack.set_child_icon_name(&library, Some("accessories-dictionary-symbolic"));
        library_stack.add_titled(&discovery, "disocvery", "Discover");
        library_stack.set_child_icon_name(&discovery, Some("dialog-information-symbolic"));

        let squeezer: libhandy::Squeezer = self.builder.get_object("squeezer").unwrap();
        let switcher_bar: libhandy::ViewSwitcherBar = self.builder.get_object("switcher_bar").unwrap();
        library_stack.connect_size_allocate(move |_, _| {
            library.queue_allocate();
            discovery.queue_allocate();
        });

        let title_wide_switcher: libhandy::ViewSwitcher = self.builder.get_object("title_wide_switcher").unwrap();
        let title_narrow_switcher: libhandy::ViewSwitcher = self.builder.get_object("title_narrow_switcher").unwrap();
        let title_label: gtk::Label = self.builder.get_object("title_label").unwrap();
        self.widget.connect_size_allocate(move |_, allocation| {
            squeezer.set_child_enabled(&title_wide_switcher, allocation.width > 600);
            squeezer.set_child_enabled(&title_label, allocation.width <= 450);
            squeezer.set_child_enabled(&title_narrow_switcher, allocation.width > 450);
            switcher_bar.set_reveal(allocation.width <= 450);
        });
    }

    fn order(&self, sort_by: Option<SortBy>, sort_order: Option<SortOrder>) {
        self.library.borrow_mut().clone().sort(sort_by.clone(), sort_order.clone());
        self.discovery.clone().borrow_mut().sort(sort_by, sort_order);
    }

    pub fn set_zoom(&self, new_zoom: f64) {
        let action_group = self.widget.get_action_group("reader").unwrap().downcast::<gio::SimpleActionGroup>().unwrap();
        let zoom_out_action = action_group.lookup_action("zoom-out").unwrap().downcast::<gio::SimpleAction>().unwrap();
        let zoom_in_action = action_group.lookup_action("zoom-in").unwrap().downcast::<gio::SimpleAction>().unwrap();
        let zoom_reset_action = action_group.lookup_action("zoom-reset").unwrap().downcast::<gio::SimpleAction>().unwrap();
        let zoom_level: gtk::Label = self.builder.get_object("zoom_level").unwrap();

        zoom_level.set_text(&format!("{:.1}%", new_zoom * 100.0));

        zoom_out_action.set_enabled(new_zoom >= 0.1);
        zoom_in_action.set_enabled(new_zoom <= 2.0);
        zoom_reset_action.set_enabled(new_zoom != 1.0);

        self.reader.borrow().set_zoom(new_zoom);
    }

    fn setup_actions(&self, s: Rc<RefCell<Self>>) {
        let actions = gio::SimpleActionGroup::new();
        let open = gio::SimpleAction::new("open", None);
        let sender = self.sender.clone();
        open.connect_activate(move |_, _| {
            sender.send(Action::OpenFile).unwrap();
        });
        actions.add_action(&open);

        let library = gio::SimpleAction::new("library", None);
        let sender = self.sender.clone();
        library.connect_activate(move |_, _| {
            sender.send(Action::ViewLibrary).unwrap();
        });
        actions.add_action(&library);

        self.widget.insert_action_group("window", Some(&actions));

        let actions = gio::SimpleActionGroup::new();
        let zoom_in = gio::SimpleAction::new("zoom-in", None);
        let window = s.clone();
        zoom_in.connect_activate(move |_, _| {
            let new_zoom = window.borrow().reader.borrow().get_zoom() + 0.1;
            window.borrow_mut().set_zoom(new_zoom);
        });
        actions.add_action(&zoom_in);

        let window = s.clone();
        let zoom_out = gio::SimpleAction::new("zoom-out", None);
        zoom_out.connect_activate(move |_, _| {
            let new_zoom = window.borrow().reader.borrow().get_zoom() - 0.1;
            window.borrow_mut().set_zoom(new_zoom);
        });
        actions.add_action(&zoom_out);

        let window = s.clone();
        let zoom_reset = gio::SimpleAction::new("zoom-reset", None);
        zoom_reset.connect_activate(move |_, _| {
            window.borrow_mut().set_zoom(1.0);
        });
        actions.add_action(&zoom_reset);
        zoom_reset.set_enabled(false); // start with zoom-reset disabledvvvvvvv

        self.widget.insert_action_group("reader", Some(&actions));

        let actions = gio::SimpleActionGroup::new();
        let sort_descending = gio::SimpleAction::new("sort-descending", None);
        let sort_ascending = gio::SimpleAction::new("sort-ascending", None);

        let window = s.clone();
        let sort_ascending_action = sort_ascending.clone();
        sort_descending.connect_activate(move |action, _| {
            action.set_enabled(false);
            sort_ascending_action.set_enabled(true);
            window.borrow_mut().order(None, Some(SortOrder::Desc));
        });
        actions.add_action(&sort_descending);

        let window = s.clone();
        let sort_descending_action = sort_descending.clone();
        sort_ascending.connect_activate(move |action, _| {
            action.set_enabled(false);
            sort_descending_action.set_enabled(true);
            window.borrow().order(None, Some(SortOrder::Asc));
        });
        actions.add_action(&sort_ascending);

        let window = s.clone();
        let sort_by = gio::SimpleAction::new("sort-by", Some(glib::VariantTy::new("s").unwrap()));
        sort_by.connect_activate(move |_, data| {
            let sort_by = SortBy::from(data.unwrap().get_str().unwrap());
            window.borrow().order(Some(sort_by), None);
        });
        actions.add_action(&sort_by);

        self.widget.insert_action_group("library", Some(&actions));
    }
}
