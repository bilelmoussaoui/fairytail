use crate::models::Paginable;
use gdk::prelude::*;
use gio::prelude::*;
use gtk::prelude::*;
use libhandy;
use libhandy::ColumnExt;
use std::{cell::RefCell, rc::Rc};

pub struct Reader {
    pub widget: libhandy::Column,
    builder: gtk::Builder,
    current_page: Rc<RefCell<usize>>,
    pixbuf: RefCell<gdk_pixbuf::Pixbuf>,

    scale: RefCell<(f64, f64)>,
    zoom: RefCell<(f64)>,
    offset: RefCell<(f64, f64)>,
    archive: RefCell<Option<Box<Paginable + 'static>>>,
}
impl Reader {
    pub fn new() -> Rc<RefCell<Self>> {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/FairyTale/reader.ui");
        let reader_widget: gtk::Box = builder.get_object("reader").unwrap();
        reader_widget.set_hexpand(true);
        let pixbuf = gdk_pixbuf::Pixbuf::new(gdk_pixbuf::Colorspace::Rgb, true, 8, 100, 100).unwrap();
        let widget = libhandy::Column::new();

        let reader = Rc::new(RefCell::new(Self {
            widget,
            builder,
            current_page: Rc::new(RefCell::new(0)),
            pixbuf: RefCell::new(pixbuf),
            scale: RefCell::new((1.0, 1.0)),
            offset: RefCell::new((0.0, 0.0)),
            zoom: RefCell::new(1.0),
            archive: RefCell::new(None),
        }));

        reader.borrow().widget.set_maximum_width(900);
        reader.borrow().widget.set_linear_growth_width(900);
        let widget = reader.borrow().widget.clone();
        let column = widget.upcast::<gtk::Widget>();
        let column = column.downcast::<gtk::Container>().unwrap();
        column.add(&reader_widget);
        column.show();
        Self::init(reader.clone());
        reader
    }

    pub fn load(&mut self, archive: Box<Paginable>) {
        let pages_adjustment: gtk::Adjustment = self.builder.get_object("pages_adjustment").unwrap();

        pages_adjustment.set_upper((archive.total_pages() - 1) as f64);
        self.archive.replace(Some(archive));
        self.load_page(0);
    }

    pub fn set_zoom(&self, zoom: f64) {
        if zoom >= 0.1 && zoom <= 2.0 {
            let drawing_area: gtk::DrawingArea = self.builder.get_object("drawing_area").unwrap();
            self.zoom.replace(zoom);
            drawing_area.queue_draw();
        }
    }

    pub fn get_zoom(&self) -> f64 {
        self.zoom.borrow().clone()
    }

    pub fn first(&self) {
        self.load_page(0);
    }

    pub fn next(&self) {
        let next_page = self.current_page.borrow().clone() + 1;
        let total_pages = self.archive.borrow().as_ref().unwrap().total_pages();
        if next_page <= total_pages {
            self.load_page(next_page);
        }
    }

    pub fn previous(&self) {
        let current_page = self.current_page.borrow().clone();
        if current_page >= 1 {
            self.load_page(current_page - 1);
        }
    }
    fn load_page(&self, page_nr: usize) {
        let page = self.archive.borrow_mut().as_mut().unwrap().get_stream(page_nr);
        if let Ok(stream) = page {
            let pixbuf = gdk_pixbuf::Pixbuf::new_from_stream(&stream, gio::NONE_CANCELLABLE).unwrap();
            self.pixbuf.replace(pixbuf);
            self.current_page.replace(page_nr);

            let area: gtk::DrawingArea = self.builder.get_object("drawing_area").unwrap();
            area.queue_allocate();
            area.queue_draw();

            let total_pages = self.archive.borrow().as_ref().unwrap().total_pages();

            let page_label: gtk::Label = self.builder.get_object("page_label").unwrap();
            page_label.set_text(&format!("Page {} out of {}", page_nr + 1, total_pages));

            let pages_adjustment: gtk::Adjustment = self.builder.get_object("pages_adjustment").unwrap();
            pages_adjustment.set_value(page_nr as f64);
        }
    }

    fn init(s: Rc<RefCell<Self>>) {
        let drawing_area: gtk::DrawingArea = s.borrow().builder.get_object("drawing_area").unwrap();
        let hadj: gtk::Adjustment = s.borrow().builder.get_object("hadjustment").unwrap();
        let vadj: gtk::Adjustment = s.borrow().builder.get_object("vadjustment").unwrap();

        let area = drawing_area.clone();
        let reader = s.clone();
        hadj.connect_value_changed(move |hadj| {
            let allocation = area.get_allocation();
            let zoom = reader.borrow().zoom.borrow().clone() * reader.borrow().scale.borrow().clone().0;

            let scaled_width = reader.borrow().pixbuf.borrow().clone().get_width() as f64 * zoom;
            let max_offset_x = scaled_width - allocation.width as f64;
            let max_value = hadj.get_upper() - hadj.get_page_size();
            let value = hadj.get_value();

            let mut offset_x = -1.0 * value * max_offset_x;
            if max_value != 0.0 {
                offset_x = offset_x / max_value;
            }
            reader.borrow().offset.borrow_mut().0 = offset_x;
            area.queue_draw()
        });
        let reader = s.clone();
        let area = drawing_area.clone();
        vadj.connect_value_changed(move |adj| {
            let allocation = area.get_allocation();
            let zoom = reader.borrow().zoom.borrow().clone() * reader.borrow().scale.borrow().clone().1;
            let scaled_height = reader.borrow().pixbuf.borrow().clone().get_height() as f64 * zoom;
            let max_offset_y = scaled_height - allocation.height as f64;
            let max_value = adj.get_upper() - adj.get_page_size();
            let value = adj.get_value();

            let mut offset_y = -1.0 * value * max_offset_y;
            if max_value != 0.0 {
                offset_y = offset_y / max_value;
            };
            reader.borrow().offset.borrow_mut().1 = offset_y;
            area.queue_draw()
        });

        let reader = s.clone();
        drawing_area.connect_draw(move |widget, ctx| {
            let scale_x = reader.borrow().scale.borrow().0 * reader.borrow().zoom.borrow().clone();
            let scale_y = reader.borrow().scale.borrow().1 * reader.borrow().zoom.borrow().clone();

            let allocation = widget.get_allocation();

            let scaled_width = reader.borrow().pixbuf.borrow().get_width() as f64 * scale_x;
            let scaled_height = reader.borrow().pixbuf.borrow().get_height() as f64 * scale_y;
            let page_size_x = allocation.width as f64 / scaled_width;

            hadj.set_lower(0.0);
            hadj.set_page_size(page_size_x);
            hadj.set_upper(1.0);
            hadj.set_step_increment(0.1);
            hadj.set_page_increment(0.5);

            let page_size_y = allocation.height as f64 / scaled_height;
            vadj.set_lower(0.0);
            vadj.set_page_size(page_size_y);
            vadj.set_upper(1.0);
            vadj.set_step_increment(0.1);
            vadj.set_page_increment(0.5);

            ctx.scale(scale_x, scale_y);
            ctx.translate(reader.borrow().offset.borrow().0, reader.borrow().offset.borrow().1);
            ctx.set_source_pixbuf(&reader.borrow().pixbuf.borrow(), 0.0, 0.0);
            ctx.paint();
            gtk::Inhibit(false)
        });
        // Rescale
        let reader = s.clone();

        let weak_reader = Rc::downgrade(&reader);
        drawing_area.connect_size_allocate(move |widget, allocation| {
            if let Some(reader) = weak_reader.upgrade() {
                //let mut scale_x = allocation.width as f64 / reader.borrow().pixbuf.borrow().get_width() as f64;
                let mut scale_y = allocation.height as f64 / reader.borrow().pixbuf.borrow().get_height() as f64;
                /*if scale_x >= 1.0 {
                    scale_x = 1.0;
                }*/
                if scale_y >= 1.0 {
                    scale_y = 1.0;
                }
                reader.borrow().scale.replace((scale_y, scale_y));
                widget.queue_draw();
            }
        });

        // Page scale
        let page_scale: gtk::Scale = s.borrow().builder.get_object("page_scale").unwrap();
        let reader = s.clone();
        page_scale.connect_change_value(move |_, _, value| {
            let page_nr = value as usize;
            reader.borrow().load_page(page_nr);
            gtk::Inhibit(false)
        });

        // Auto/Hide controls
        let overlay: gtk::Overlay = s.borrow().builder.get_object("overlay").unwrap();

        overlay.add_events(gdk::EventMask::ENTER_NOTIFY_MASK | gdk::EventMask::LEAVE_NOTIFY_MASK);
        drawing_area.add_events(gdk::EventMask::SCROLL_MASK);

        let control_revealer: gtk::Revealer = s.borrow().builder.get_object("control_revealer").unwrap();
        let control_rev = control_revealer.clone();
        overlay.connect_enter_notify_event(move |_, _| {
            control_rev.set_reveal_child(true);
            gtk::Inhibit(false)
        });
        overlay.connect_leave_notify_event(move |_, _| {
            control_revealer.set_reveal_child(false);
            gtk::Inhibit(false)
        });
        // Ctrl + scroll up/down to zoom/unzoom
        let reader = s.clone();
        drawing_area.connect_scroll_event(move |_, event| {
            if event.get_direction() == gdk::ScrollDirection::Up {
                if event.get_state() == gdk::ModifierType::CONTROL_MASK {
                    reader.borrow().set_zoom(reader.borrow().get_zoom() - 0.1);
                } else {
                    reader.borrow().next();
                }
            } else {
                if event.get_state() == gdk::ModifierType::CONTROL_MASK {
                    reader.borrow().set_zoom(reader.borrow().get_zoom() + 0.1);
                } else {
                    reader.borrow().previous();
                }
            }
            gtk::Inhibit(false)
        });

        // Setup actions
        let actions = gio::SimpleActionGroup::new();
        let page = gio::SimpleAction::new("page", Some(glib::VariantTy::new("s").unwrap()));
        let reader = s.clone();
        page.connect_activate(move |_, data| match data.unwrap().get_str() {
            Some(page) => match page {
                "next" => reader.borrow().next(),
                "previous" => reader.borrow().previous(),
                _ => reader.borrow().first(),
            },
            _ => {
                reader.borrow().first();
            }
        });
        actions.add_action(&page);
        let reader_widget: gtk::Box = s.borrow().builder.get_object("reader").unwrap();
        reader_widget.insert_action_group("reader", Some(&actions));
    }
}
