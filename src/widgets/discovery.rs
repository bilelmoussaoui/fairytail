use super::library_flowbox::LibraryFlowBox;
use super::library_row::LibraryRow;
use crate::api::WebResource;
use crate::application::Action;
use crate::models::{DiscoveryModel, ObjectWrapper};
use crate::models::{SortBy, SortOrder};
use glib::Sender;
use gtk::prelude::*;

pub struct Discovery {
    pub widget: libhandy::Column,
    builder: gtk::Builder,
    model: DiscoveryModel,
    sender: Sender<Action>,
}

impl Discovery {
    pub fn new(sender: Sender<Action>) -> Self {
        let model = DiscoveryModel::new();
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/FairyTale/discovery.ui");
        let widget = builder.get_object("discovery").expect("Failed to load library object");

        let library = Discovery { widget, builder, model, sender };
        library.init();
        library
    }

    fn init(&self) {
        let discovery_container: gtk::Box = self.builder.get_object("discovery_container").unwrap();
        let manga_flowbox = LibraryFlowBox::new_with_label("Manga", self.sender.clone());
        let comics_flowbox = LibraryFlowBox::new_with_label("Comics", self.sender.clone());
        let sender = self.sender.clone();
        let on_bind_model = move |viewable: &glib::Object| {
            let viewable: WebResource = viewable.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
            let row = LibraryRow::new(Box::new(viewable.clone()), sender.clone());
            let sender = sender.clone();
            row.set_on_click_callback(move |_, _| {
                // sender.send(Action::LoadChapter(chapter.clone())).unwrap();
                gtk::Inhibit(false)
            });
            row.widget.upcast::<gtk::Widget>()
        };
        manga_flowbox.bind_model(&self.model.manga_model, on_bind_model.clone());
        comics_flowbox.bind_model(&self.model.comics_model, on_bind_model);
        discovery_container.add(&comics_flowbox.widget);
        discovery_container.add(&manga_flowbox.widget);
    }

    pub fn sort(&self, sort_by: Option<SortBy>, sort_order: Option<SortOrder>) {
        self.model.set_sorting(sort_by, sort_order);
    }
}
