table! {
    chapters (id) {
        id -> Integer,
        title -> Text,
        path -> Text,
        cover_path -> Text,
        created_date -> Text,
    }
}
