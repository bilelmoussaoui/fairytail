use std::ffi::OsStr;
use std::path::Path;

pub fn is_image(filename: &str) -> bool {
    // Check the extension of the file whether it's a png/jpeg
    let extension = Path::new(filename).extension().and_then(OsStr::to_str);
    match extension {
        Some(ext) => ext == "png" || ext == "jpeg" || ext == "jpg",
        None => false,
    }
}
