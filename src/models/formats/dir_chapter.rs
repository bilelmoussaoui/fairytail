use super::utils;
use super::Error;
use crate::models::chapter::Paginable;
use std::boxed::Box;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::path::Path;

pub struct DirChapter {
    current_page: usize,
    pages: Vec<Box<std::path::PathBuf>>, // Index in zip, the file name
}

impl DirChapter {
    pub fn new(chapter_path: String) -> Result<Self, Error> {
        let path = Path::new(&chapter_path);
        let mut pages: Vec<Box<std::path::PathBuf>> = Vec::new();

        for entry in fs::read_dir(path)? {
            let entry = entry?;
            let file_path = entry.path();
            if !file_path.is_dir() && utils::is_image(file_path.to_str().unwrap()) {
                pages.push(Box::new(file_path));
            }
        }
        pages.sort_by(|a, b| a.to_str().cmp(&b.to_str()));

        let chapter = DirChapter { pages, current_page: 0 };
        Ok(chapter)
    }
}

impl Paginable for DirChapter {
    fn get_page(&mut self, page_nr: usize) -> Result<glib::Bytes, Error> {
        match &self.pages.get(page_nr) {
            Some(path) => {
                let mut file = File::open(path.to_str().unwrap())?;
                let mut bytes: Vec<u8> = Vec::new();
                file.read_to_end(&mut bytes)?;
                Ok(glib::Bytes::from(&bytes))
            }
            None => bail!("Page not found"),
        }
    }

    fn get_current_page(&self) -> usize {
        self.current_page
    }
    fn total_pages(&self) -> usize {
        self.pages.len() as usize
    }
}
