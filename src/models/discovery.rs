use super::library::{SortBy, SortOrder};
use super::object_wrapper::ObjectWrapper;
use crate::api::{get_top_comics, get_top_manga, WebResource};
use crate::models::Viewable;
use gio::prelude::*;
use glib::prelude::*;
use std::cell::RefCell;

pub struct DiscoveryModel {
    pub manga_model: gio::ListStore,
    pub comics_model: gio::ListStore,
    sort_order: RefCell<SortOrder>,
    sort_by: RefCell<SortBy>,
}

impl DiscoveryModel {
    pub fn new() -> Self {
        let comics_model = gio::ListStore::new(ObjectWrapper::static_type());
        let manga_model = gio::ListStore::new(ObjectWrapper::static_type());
        let model = Self {
            comics_model,
            manga_model,
            sort_order: RefCell::new(SortOrder::Asc),
            sort_by: RefCell::new(SortBy::Name),
        };
        model.init();
        model
    }

    fn init(&self) {
        // fill in the chapters from the database
        for manga in get_top_manga().unwrap().into_iter() {
            self.add_resource(&manga, &self.manga_model);
        }

        for comics in get_top_comics().unwrap().into_iter() {
            self.add_resource(&comics, &self.comics_model);
        }
    }
    fn add_resource(&self, resource: &WebResource, model: &gio::ListStore) {
        let object = ObjectWrapper::new(resource);
        let sort_by = self.sort_by.clone();
        let sort_order = self.sort_order.clone();
        model.insert_sorted(&object, move |a, b| Self::resources_cmp(a, b, sort_by.borrow().clone(), sort_order.borrow().clone()));
    }
    pub fn set_sorting(&self, sort_by: Option<SortBy>, sort_order: Option<SortOrder>) {
        let sort_by = match sort_by {
            Some(sort_by) => {
                self.sort_by.replace(sort_by.clone());
                sort_by
            }
            None => self.sort_by.borrow().clone(),
        };

        let sort_order = match sort_order {
            Some(sort_order) => {
                self.sort_order.replace(sort_order.clone());
                sort_order
            }
            None => self.sort_order.borrow().clone(),
        };
        let sort = (sort_by.clone(), sort_order.clone());
        self.comics_model.sort(move |a, b| Self::resources_cmp(a, b, sort.0.clone(), sort.1.clone()));
        self.manga_model.sort(move |a, b| Self::resources_cmp(a, b, sort_by.clone(), sort_order.clone()));
    }

    fn resources_cmp(a: &gio::Object, b: &gio::Object, sort_by: SortBy, sort_order: SortOrder) -> std::cmp::Ordering {
        let mut resource_a: WebResource = a.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
        let mut resource_b: WebResource = b.downcast_ref::<ObjectWrapper>().unwrap().deserialize();

        if sort_order == SortOrder::Desc {
            let tmp = resource_a;
            resource_a = resource_b;
            resource_b = tmp;
        }

        match sort_by {
            SortBy::Name => resource_a.get_title().cmp(&resource_b.get_title()),
            SortBy::Date => resource_a.get_created_at().cmp(&resource_b.get_created_at()),
        }
    }
}
