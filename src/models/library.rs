use super::chapter::{Chapter, NewChapter};
use super::database;
use super::database::Error;
use super::database::Insert;
use super::object_wrapper::ObjectWrapper;
use crate::models::Viewable;
use gio::prelude::*;
use glib::prelude::*;
use std::cell::RefCell;

#[derive(Clone, PartialEq)]
pub enum SortOrder {
    Asc,
    Desc,
}

#[derive(Clone, PartialEq)]
pub enum SortBy {
    Name,
    Date,
}

impl From<&str> for SortBy {
    fn from(sortby: &str) -> Self {
        match sortby {
            "name" => SortBy::Name,
            "date" => SortBy::Date,
            _ => SortBy::Name,
        }
    }
}

pub struct LibraryModel {
    pub model: gio::ListStore,
    sort_order: RefCell<SortOrder>,
    sort_by: RefCell<SortBy>,
}

impl LibraryModel {
    pub fn new() -> Self {
        let gio_model = gio::ListStore::new(ObjectWrapper::static_type());
        let model = Self {
            model: gio_model,
            sort_order: RefCell::new(SortOrder::Desc),
            sort_by: RefCell::new(SortBy::Name),
        };
        model.init();
        model
    }

    pub fn add_item(&self, chapter: NewChapter) -> Result<Chapter, Error> {
        let chapter = chapter.insert()?;
        self.add_chapter(&chapter);
        Ok(chapter)
    }

    fn init(&self) {
        // fill in the chapters from the database
        let chapters = database::get_chapters().unwrap();

        for chapter in chapters.into_iter() {
            self.add_chapter(&chapter);
        }
    }

    fn add_chapter(&self, chapter: &Chapter) {
        let object = ObjectWrapper::new(Box::new(chapter));
        let sort_by = self.sort_by.clone();
        let sort_order = self.sort_order.clone();
        self.model
            .insert_sorted(&object, move |a, b| Self::chapters_cmp(a, b, sort_by.borrow().clone(), sort_order.borrow().clone()));
    }

    pub fn get_count(&self) -> u32 {
        self.model.get_n_items()
    }

    pub fn set_sorting(&self, sort_by: Option<SortBy>, sort_order: Option<SortOrder>) {
        let sort_by = match sort_by {
            Some(sort_by) => {
                self.sort_by.replace(sort_by.clone());
                sort_by
            }
            None => self.sort_by.borrow().clone(),
        };

        let sort_order = match sort_order {
            Some(sort_order) => {
                self.sort_order.replace(sort_order.clone());
                sort_order
            }
            None => self.sort_order.borrow().clone(),
        };
        self.model.sort(move |a, b| Self::chapters_cmp(a, b, sort_by.clone(), sort_order.clone()));
    }

    fn chapters_cmp(a: &gio::Object, b: &gio::Object, sort_by: SortBy, sort_order: SortOrder) -> std::cmp::Ordering {
        let mut chapter_a: Chapter = a.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
        let mut chapter_b: Chapter = b.downcast_ref::<ObjectWrapper>().unwrap().deserialize();

        if sort_order == SortOrder::Desc {
            let tmp = chapter_a;
            chapter_a = chapter_b;
            chapter_b = tmp;
        }

        match sort_by {
            SortBy::Name => chapter_a.get_title().cmp(&chapter_b.get_title()),
            SortBy::Date => chapter_a.get_created_at().cmp(&chapter_b.get_created_at()),
        }
    }
}
