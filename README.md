<a href="https://flathub.org/apps/details/com.belmoussaoui.FairyTale">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Fairy Tale

<img src="https://gitlab.gnome.org/belmoussaoui/fairytale/raw/master/data/icons/com.belmoussaoui.FairyTale.svg" width="128" height="128" />

Read and manage your comics collection.


## Screenshots

<div align="center">
![screenshot](data/resources/screenshots/screenshot1.png)
</div>

## Hack on Fairy Tale
To build the development version of  Fairy Tale and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow our [Code of Conduct](/code-of-conduct.md) when participating in project
spaces.
